# gentoo-tpx220
Various files related to running Gentoo on a Thinkpad X220

This is mostly for my own use, but others may find it helpful. The kernel configuration should, to the best of my knowledge, enable all hardware and features on the X220. I haven't tested the fingerprint sensor, but it is detected by `lsusb`.

This kernel is built for use with systemd. Cope and seethe.
